CPT111 - Building IT Systems
RMIT University
OUA Study Period 3 2016

------------------------------------------
GROUP DETAILS
------------------------------------------
Name: Bits Please!

Members: Evan Le Clercq - S3516635
	 Scott Cline - S3241116
	 Luke Overton - S3571432
	 David Ngo - S3569505

Mentor: Matthew Cameron

------------------------------------------
GAME DETAILS
------------------------------------------

Idea: Top-down Vertical Scrolling Shooter

Reference:	Raiden Games (Classic Arcade Games)
		Sky Force (Android/iOS Game)

Description:
Endless Top-down vertical scrolling shooter based upon that classic arcade games like Raiden. 

------------------------------------------
GAME FEATURES
------------------------------------------
Minimum Viable Product Features:
	Game Menus (Main, Help, Game Over)
	Controllable Player Sprite
	Player Sprite can fire Projectiles	
	Scrolling Background (Vertical Top to Bottom)
	Enemies spawn and attack player
	Player can respawn
	Player Sprite can detect collisions with other sprites
	Game Keeps the players score during play

Extended Features Implemented:
	Environmental Obstacles
	Weapon and Score pickups
	Game ends after player loses all of their lives.

------------------------------------------
LOAD INTO UNITY
------------------------------------------
1. Open Unity
2. Wait until the Projects window is open
3. Click on the 'OPEN' button on top rightof window
4. Navigate to the location you saved the project files
5. Click on the folder containing the files.
6. Press the 'Select Folder' Button
	Unity will import all of the assests to the project
7. Open the 'scenes' folder in the Unity Editor
8. Open the 'Start_Menu' scene
9. Press the 'Play' Button along the top toolbar to start game.