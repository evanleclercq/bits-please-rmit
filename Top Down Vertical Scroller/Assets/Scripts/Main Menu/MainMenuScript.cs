﻿
///Main Menu.
///Attached to Main Camera
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {
    public Texture backgroundTexture;

    public float guiPlacmentY1;
    public float guiPlacmentY2;
    public float guiPlacmentY3;

    public float guiPlacmentX1;
    public float guiPlacmentX2;
    public float guiPlacmentX3;

    public string DestinationGame;
    public string DestinationHelp;

    void OnGUI()
    {
        /// Display Background
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture);

        ///Displays our Buttons
        if (GUI.Button(new Rect(Screen.width*guiPlacmentX1, Screen.height*guiPlacmentY1, Screen.width * .4f, Screen.height * .08f), "Play Game"))
        {

            SceneManager.LoadScene(DestinationGame);
        }
        if (GUI.Button(new Rect(Screen.width*guiPlacmentX2, Screen.height*guiPlacmentY2, Screen.width * .4f, Screen.height * .08f), "Controls"))
        {
            SceneManager.LoadScene(DestinationHelp);
        }
        if (GUI.Button(new Rect(Screen.width*guiPlacmentX3, Screen.height*guiPlacmentY3, Screen.width * .4f, Screen.height * .08f), "Exit"))
        {
            
                Application.Quit();
            
        }
    }
}