﻿using UnityEngine;
using System.Collections;

public class UpgradePickUp : MonoBehaviour {


    private GameController gameContoller;
    public int weaponUpgrade;


    void Start()
    {
        GameObject gameContollerObject = GameObject.FindWithTag("GameController");
        if (gameContollerObject != null)
        {
            gameContoller = gameContollerObject.GetComponent<GameController>();
        }
        if (gameContoller == null)
        {
            Debug.Log("Cannot find GameContoller Script");
        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Boundary")
        {

            return;
        }


        gameContoller.upWeapon(weaponUpgrade);
        Destroy(gameObject);
    }
}