﻿using UnityEngine;
using System.Collections;

public class DestroyPlayerByShot : MonoBehaviour {

	public GameObject playerExplosion;
	private GameController gameController;
	public AudioClip sound;
	private AudioSource audio;

	void Start()
	{
		GameObject gameContollerObject = GameObject.FindWithTag("GameController");
		if (gameContollerObject != null)
		{
			gameController = gameContollerObject.GetComponent<GameController>();
		}
		if (gameController == null)
		{
			Debug.Log("Cannot find GameContoller Script");
		}
		audio = new AudioSource ();
	}

	void OnTriggerEnter2D(Collider2D other){

		if (other.tag == "EProjectile") {
			print ("Player Hit");
			Instantiate (playerExplosion, transform.position, transform.rotation);
//			audio.PlayOneShot (sound);
			gameController.takeLife();
			Destroy (other.gameObject);
			gameController.respawn (gameObject);
		}


	}
}
