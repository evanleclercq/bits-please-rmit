﻿using UnityEngine;
using System.Collections;

public class DestroyOnCollide : MonoBehaviour {

    private GameController gameController;
	public GameObject explosion;
    public int scoreValue;
	public AudioSource audio;
	public AudioClip sound;


    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find GameContoller Script");
        }

		audio = GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Boundary") {
			return;
		}
		if (other.tag == "EProjectile") {
			return;
		}

        gameController.AddScore(scoreValue);


		if (other.tag == "Player") {
			gameController.respawn (other.gameObject);
			Instantiate (explosion, other.transform.position, other.transform.rotation);
			gameController.takeLife ();
		} else {
			Destroy (other.gameObject);
		}
		Instantiate (explosion, transform.position, transform.rotation);
//		audio.PlayOneShot(audio.clip);
		Destroy (gameObject);
//		Destroy (other.gameObject);


	}
}
