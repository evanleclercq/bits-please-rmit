﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary
{
	public float xMin, xMax, yMin, yMax;
}

public class PlayerController : MonoBehaviour
{
	public float speed;
	public float tilt;
	public Boundary boundary;
	public GameObject projectile;
    public GameObject projectile2;
    public GameObject projectile3;
    public Transform projectileSpawn;
	public float fireRate;
	private float fireTime;


    public int weaponGrade;

    private GameController gameContoller;
   


    void Start()
    {
        GameObject gameContollerObject = GameObject.FindWithTag("GameController");
        if (gameContollerObject != null)
        {
            gameContoller = gameContollerObject.GetComponent<GameController>();
        }
        if (gameContoller == null)
        {
            Debug.Log("Cannot find GameContoller Script");
        }
    }
    void Update () {

//		Input.GetKey (KeyCode.UpArrow)
//
//		if (Input.GetAxis("Vertical") > 0) {
//			playerMove (0, speed);
//		} else if (Input.GetAxis("Vertical") < 0) {
//			playerMove (0, -speed);
//		}
//
//		if (Input.GetAxis("Horizontal") > 0) {
//			playerMove (speed, 0);
//		} else if (Input.GetAxis("Horizontal") < 0) {
//			playerMove (-speed, 0);
//		}

		if (Input.GetButton ("Fire1")) {
			playerFire ();
		}

		if (Input.GetButton ("Fire2")) {
			playerBomb ();
		}

		if (Input.GetButton ("Fire3")) {
			playerSpecial ();
		}

	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector2 movement = new Vector2 (moveHorizontal,  moveVertical);
		GetComponent<Rigidbody2D>().velocity = movement * speed;

		GetComponent<Rigidbody2D>().position = new Vector2
			(
				Mathf.Clamp (GetComponent<Rigidbody2D>().position.x, boundary.xMin, boundary.xMax), 
				Mathf.Clamp (GetComponent<Rigidbody2D>().position.y, boundary.yMin, boundary.yMax)
			);


	}

	void playerMove (float x, float y) {

		Vector3 playPos = transform.position;

		if (playPos.x < Screen.width && playPos.y < Screen.height) {
			transform.Translate (new Vector3 (x, y, 0) * Time.deltaTime);
		}
	
	}

 


    void playerFire()
    {
        weaponGrade = gameContoller.weaponGrade();
        //print ("FIRE!");
        if (weaponGrade == 0)
        {
            if (Time.time > fireTime)
            {
                fireTime = Time.time + fireRate;
                Instantiate(projectile, projectileSpawn.position, projectileSpawn.rotation);
            }
        }
        if (weaponGrade == 1)
        {
            if (Time.time > fireTime)
            {
                fireTime = Time.time + fireRate;
                Instantiate(projectile2, projectileSpawn.position, projectileSpawn.rotation);
            }
        }
        if (weaponGrade > 1)
        {
            if (Time.time > fireTime)
            {
                fireTime = Time.time + fireRate;
                Instantiate(projectile3, projectileSpawn.position, projectileSpawn.rotation);
            }
        }


    }



    void playerBomb () {
		print ("Bomb Dropped!");
		//TODO: Implelment bomb attack
	}

	void playerSpecial () {
		print ("Special Used!");
		//TODO: Implement Special Attack
	}

}
