﻿using UnityEngine;
using System.Collections;

public class ScoreOnCollide : MonoBehaviour {
    private GameController gameContoller;
    public int scoreValue;


    void Start ()
    {
        GameObject gameContollerObject = GameObject.FindWithTag("GameController");
        if(gameContollerObject != null)
        {
            gameContoller = gameContollerObject.GetComponent<GameController>();
        }
        if (gameContoller == null)
        {
            Debug.Log("Cannot find GameContoller Script");
        }
    }
    

        void OnTriggerEnter2D(Collider2D other)
        {

        if (other.tag == "Boundary")
        {

            return;
        }


        gameContoller.AddScore(scoreValue);
            Destroy(gameObject);
        }
    }
