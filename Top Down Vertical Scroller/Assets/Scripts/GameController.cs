﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	public GameObject enemy1;
	public GameObject enemy2;
	public GameObject asteroid;
	public Vector3 spawnValues;
	public int enemyCount;
	public float timeWait;
	public float beginWait;
	public float gapWait;
	public AudioSource backgroundMusic;

    public GameObject ScorePickuP;
    public Vector3 scorespawnValues;
    public int ScoreCount;
    public float ScoretimeWait;
    public float scorebeginWait;
    public float scoregapWait;
	public float timeAlive;

    public GameObject weaponPickuP;
    public Vector3 weaponspawnValues;
    public int weaponCount;
    public float weapontimeWait;
    public float weaponbeginWait;
    public float weapongapWait;

    public GUIText scoreText;
	public GUIText lifeText;
	public GUIText timeAliveText;
    public int score;
	public int lives;

	public int weaponLevel;

	// Use this for initialization
	void Start () {
        score = 0;
        UpdateScore();
		updateLives ();
        StartCoroutine(SpawnScorePickUps());
        StartCoroutine (SpawnEnemies ());
        StartCoroutine(SpawnWeaponPickUps());
		backgroundMusic.Play();


    }
    
	void Update () {
		timeAlive = timeAlive + Time.deltaTime;
		timeAliveText.text = "Time Alive: " + ((int)(timeAlive/60)) + ":" + (timeAlive%60).ToString("F2");
	}
	
	// Update is called once per frame
	IEnumerator SpawnEnemies () {
		GameObject hazard;
		yield return new WaitForSeconds (beginWait);
		while (true) {
			for (int i = 0; i < enemyCount; i++) {

				int randomNumber = Random.Range (0, 100);

				if (randomNumber <= 60) {
					hazard = enemy1;
				} else if (randomNumber > 60) {
					hazard = asteroid;
				} else {
					hazard = enemy1;
				}

				Vector3 EnemyPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion enemyRotation = Quaternion.identity;
				Instantiate (hazard, EnemyPosition, enemyRotation);
				yield return new WaitForSeconds (timeWait);
			}
			yield return new WaitForSeconds (gapWait);
		}
	}

    // Update is called once per frame
    IEnumerator SpawnScorePickUps()
    {

        yield return new WaitForSeconds(scorebeginWait);
        while (true)
        {
            for (int i = 0; i < ScoreCount; i++)
            {

                Vector3 ScorePickUpPosition = new Vector3(Random.Range(-scorespawnValues.x, scorespawnValues.x), scorespawnValues.y, scorespawnValues.z);
                Quaternion ScorePickUpRotation = Quaternion.identity;
                Instantiate(ScorePickuP, ScorePickUpPosition, ScorePickUpRotation);
                yield return new WaitForSeconds(ScoretimeWait);
            }
            yield return new WaitForSeconds(scoregapWait);
        }
    }

    IEnumerator SpawnWeaponPickUps()
    {

        yield return new WaitForSeconds(weaponbeginWait);
        while (true)
        {
            for (int i = 0; i < weaponCount; i++)
            {

                Vector3 weaponPickUpPosition = new Vector3(Random.Range(-weaponspawnValues.x, weaponspawnValues.x), weaponspawnValues.y, weaponspawnValues.z);
                Quaternion weaponPickUpRotation = Quaternion.identity;
                Instantiate(weaponPickuP, weaponPickUpPosition, weaponPickUpRotation);
                yield return new WaitForSeconds(weapontimeWait);
            }
            yield return new WaitForSeconds(weapongapWait);
        }
    }


    // A public fnction to upadate the score value when an enemy vessle is destroyed
    public void AddScore (int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

	public void takeLife () {
		this.lives--;
		if (lives == 0) {
			Application.LoadLevel (3);
		} else {
			updateLives ();
		}
	}

	public void updateLives () {
		lifeText.text = "Lives: " + lives;
	}

    void UpdateScore ()
    {
        scoreText.text = "Score: " + score; 
    }

    // Public function to upgrade weapon
    public void upWeapon(int weaponUp)
    {
        weaponLevel += weaponUp;   
    }
    public int weaponGrade()
    {
        return weaponLevel;
    }

	public void respawn(GameObject player) {
		player.transform.position = new Vector3 (0, 0, 0);
		this.weaponLevel = 0;
	}

}
