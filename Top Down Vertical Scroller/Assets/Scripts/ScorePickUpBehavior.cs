﻿using UnityEngine;
using System.Collections;

public class ScorePickUpBehavior : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rb;


    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = (transform.up * speed);


    }
}