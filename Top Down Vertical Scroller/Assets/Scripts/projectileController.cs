﻿using UnityEngine;
using System.Collections;

public class projectileController : MonoBehaviour {

	public float speed;
	public Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();


	}

	void FixedUpdate(){

		//rb.AddForce (transform.up * speed);
		rb.velocity = (transform.up * speed);
	}


}
