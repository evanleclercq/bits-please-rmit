﻿using UnityEngine;
using System.Collections;

public class Enemy2Controller : MonoBehaviour {

	public float speed;
	public Rigidbody2D rb;
	public GameObject projectile;
	public Transform projectileSpawn;
	public float fireRate;
	private float fireTime;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();

	}

	// Update is called once per frame
	void FixedUpdate () {
		rb.velocity = (transform.up * speed);

		if (Time.time > fireTime) {
			fireTime = Time.time + fireRate;
			enemyFire ();
		}

	}

	void enemyFire () {
		//print ("FIRE!");

		Instantiate (projectile, projectileSpawn.position, projectileSpawn.rotation);

	}
}
