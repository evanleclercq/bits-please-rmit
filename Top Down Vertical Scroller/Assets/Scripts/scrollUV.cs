﻿// This will work best for images that have a repeating pattern.

using UnityEngine;
using System.Collections;

public class scrollUV : MonoBehaviour {

	//Number of seconds takes for the whole background to pass once.
	public int scrollSpeed = 5;


	// Update is called once per frame
	void Update () {

		//Gets the required variables for the method.
		MeshRenderer mr = GetComponent<MeshRenderer> ();
		Material mat = mr.material;
		Vector2 offset = mat.mainTextureOffset;

		//Change the offset of the texture by time. 
		offset.y += Time.deltaTime / scrollSpeed;
		mat.mainTextureOffset = offset;
	
	}
}
