﻿///Help Menu.
///Attached to Main Camerausing UnityEngine;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour
{

	public Texture helpBackgroundTexture;

	public string DestinationMenu;

	public float guiPlacmentY1;
	public float guiPlacmentX1;

	void OnGUI()
	{
		/// Display Background
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), helpBackgroundTexture);


		///Displays our Buttons
		if (GUI.Button(new Rect(Screen.width * guiPlacmentX1, Screen.height * guiPlacmentY1, Screen.width * .4f, Screen.height * .08f), "Main Menu"))
		{

			SceneManager.LoadScene(DestinationMenu);
		}
	}
}