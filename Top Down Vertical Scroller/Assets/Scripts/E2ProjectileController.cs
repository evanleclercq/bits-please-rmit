﻿using UnityEngine;
using System.Collections;

public class E2ProjectileController : MonoBehaviour {

	public float speed;
	public Rigidbody2D rb;
	private Vector2 startPos;
	private Vector2 endPos;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		startPos = transform.position;
		endPos = player.transform.position;

	}

	void FixedUpdate(){
		transform.position = Vector2.Lerp(startPos, endPos, (speed * Time.deltaTime));
	}

}
